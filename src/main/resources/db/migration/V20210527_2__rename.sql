ALTER TABLE message
    RENAME COLUMN isRead to is_read;

ALTER table message
    RENAME COLUMN  readTimestamp to read_timestamp;

ALTER TABLE  message
    RENAME COLUMN isDelivered to is_delivered;

ALTER TABLE  message
    RENAME COLUMN  deliveredTimestamp to delivered_timestamp;