ALTER TABLE message
ADD COLUMN created_timestamp bigint ;

ALTER TABLE message
ADD COLUMN updated_timestamp bigint ;