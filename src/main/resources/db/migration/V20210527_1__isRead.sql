ALTER TABLE message
ADD COLUMN isRead boolean;

ALTER table message
ADD COLUMN  readTimestamp bigint;

ALTER TABLE  message
ADD COLUMN isDelivered boolean;

ALTER TABLE  message
ADD COLUMN  deliveredTimestamp bigint;