package kz.aitu.chat1906.service;

import kz.aitu.chat1906.model.Message;
import kz.aitu.chat1906.repository.IAuthRepository;
import kz.aitu.chat1906.repository.IMessageRepository;
import kz.aitu.chat1906.repository.IParticipantRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class MessageService {

    private IMessageRepository _messageRepository;
    private IParticipantRepository _participantRepository;
    private  IAuthRepository _authRepository;

    public List<Message> getAll(Long userId, String token) throws Exception {

        if (_authRepository.existsByToken(token)) {
            List<Message> allMessages = _messageRepository.findAll();
            allMessages.forEach(message -> {
                if (message.getUserId() != userId) {
                    message.setRead(true);
                    message.setReadTimestamp(getCurrentDate());
                    message.setDelivered(true);
                    message.setDeliveredTimestamp(getCurrentDate());
                    _messageRepository.save(message);
                }
            });

            return allMessages;
        }else {
            throw new Exception("ERROR : TOKEN is INCORRECT");
        }

    }

    public List<Message> getNotDelivered(String token) throws Exception {
        if (_authRepository.existsByToken(token)) {
            return _messageRepository.getByDeliveredIsFalse();
        }else {
            throw new Exception("ERROR : TOKEN is INCORRECT");
        }
    }

    public Message changeIsReadStatus(Long userId, Long messageId, Boolean status,String token) throws Exception {


        if (_authRepository.existsByToken(token)) {
            Message message = _messageRepository.findMessageByUserIdAndId(userId, messageId);
            message.setReadTimestamp(null);
            message.setRead(status);
            return message;
        }else {
            throw new Exception("ERROR : TOKEN is INCORRECT");
        }
    }

    public List<Message> getMessagesByChatId(Long id, Integer size,String token) throws Exception {
        if (_authRepository.existsByToken(token)) {
            return _messageRepository.findByChatId(id, PageRequest.of(1, size, Sort.by(Sort.Direction.DESC, "CreatedTimestamp")).first());
        }else {
            throw new Exception("ERROR : TOKEN is INCORRECT");
        }
    }

    public Message addMessage(Message message, String token) throws Exception {

        if (_authRepository.existsByToken(token)) {
            if (message.getText().isEmpty() && message.getText().isBlank()) throw new Exception("message text is null");

            if (!_participantRepository.existsByUserIdAndChatId(message.getUserId(), message.getChatId()))
                throw new Exception("chat with this user not exist");

            if (message.getUserId() == null && message.getChatId() == null) throw new Exception("userId && chatId is null");

            message.setCreatedTimestamp(getCurrentDate());

            _messageRepository.save(message);
            return _messageRepository.save(message);
        }else {
            throw new Exception("ERROR : TOKEN is INCORRECT");
        }

    }

    private Long getCurrentDate() {
        Date date = new Date();
        return date.getTime();
    }

    public String deleteMessageById(Long id, String token) throws Exception {

        if (_authRepository.existsByToken(token)) {
            _messageRepository.deleteById(id);
            return "Succesfull deleted";

        }else {
            throw new Exception("ERROR : TOKEN is INCORRECT");
        }
    }

    public Message updateMessageById(Message message, String token) throws Exception {

        if (_authRepository.existsByToken(token)) {
            if (message.getId() == null) throw new Exception("message id is null");

            if (message.getText() == null || message.getText().isEmpty()) throw new Exception("text is null");

            Optional<Message> checkMessage = _messageRepository.findById(message.getId());
            if (checkMessage.isEmpty()) throw new Exception("message is not exist");

            message.setCreatedTimestamp(checkMessage.get().getCreatedTimestamp());

            message.setUpdatedTimestamp(getCurrentDate());
            _messageRepository.save(message);

            return _messageRepository.save(message);

        }else {
            throw new Exception("ERROR : TOKEN is INCORRECT");
        }
    }

    public List<Message> getLastTenMessageByChatId(Long id, String token) throws Exception {
        if (_authRepository.existsByToken(token)) {
            List<Message> messages = _messageRepository.getLastTenMessagesByChatId(id);
            messages.forEach(message -> {
                message.setRead(true);
                message.setReadTimestamp(getCurrentDate());
                message.setDelivered(true);
                message.setDeliveredTimestamp(getCurrentDate());
                _messageRepository.save(message);
            });
            return messages;
        }else {
            throw new Exception("ERROR : TOKEN is INCORRECT");
        }
    }

    public List<Message> getLastTenMessageByUserId(Long id,String token) throws Exception {

        if (_authRepository.existsByToken(token)) {
            List<Message> messages =_messageRepository.getLastTenMessageByUserId(id);
            messages.forEach(message -> {
                message.setRead(true);
                message.setReadTimestamp(getCurrentDate());
                message.setDelivered(true);
                message.setDeliveredTimestamp(getCurrentDate());
                _messageRepository.save(message);
            });
            return messages;
        }else {
            throw new Exception("ERROR : TOKEN is INCORRECT");
        }
    }
}
