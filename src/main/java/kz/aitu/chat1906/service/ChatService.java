package kz.aitu.chat1906.service;

import kz.aitu.chat1906.model.Chat;

import kz.aitu.chat1906.model.Participant;
import kz.aitu.chat1906.model.modelDTO.ReturnAuthDTO;
import kz.aitu.chat1906.repository.IAuthRepository;
import kz.aitu.chat1906.repository.IChatRepository;
import kz.aitu.chat1906.repository.IParticipantRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ChatService {
    private IChatRepository _chatRepository;
    private IParticipantRepository _participantRepository;
    private IAuthRepository _authRepository;

    public List<Chat> getChats(String token) throws Exception {

        if (_authRepository.existsByToken(token)) {
            return _chatRepository.findAll();
        }else {
            throw new Exception("ERROR : TOKEN is INCORRECT");
        }
    }

    public List<Chat> getChatsByUserId( Long id, String token) throws Exception {

        if (_authRepository.existsByToken(token)) {
            List<Participant> participantList =  _participantRepository.findParticipantsByUserId(id);
            List<Chat> chatList = new LinkedList<>();
            for (Participant participant : participantList){
                Optional<Chat> optionalChat =  _chatRepository.findById(participant.getChatId());
                optionalChat.ifPresent(chatList::add);
            }
            return chatList;
        }else {
            throw new Exception("ERROR : TOKEN is INCORRECT");
        }
    }

    public Chat addChat(Chat chat, String token) throws Exception {

        if (_authRepository.existsByToken(token)) {
            return _chatRepository.save(chat);
        }else {
            throw new Exception("ERROR : TOKEN is INCORRECT");
        }
    }

    public String deleteChatById(Long id, String token) throws Exception {

        if (_authRepository.existsByToken(token)) {
            _chatRepository.deleteById(id);
            return "Succesfully deleted";
        }else {
            throw new Exception("ERROR : TOKEN is INCORRECT");
        }

    }

    public String deleteAll(String token) throws Exception {

        if (_authRepository.existsByToken(token)) {
            _chatRepository.deleteAll();
            return "Deleted";
        }else {
            throw new Exception("ERROR : TOKEN is INCORRECT");
        }
    }

    public Chat updateById( Chat chat, String token) throws Exception {
        if (_authRepository.existsByToken(token)) {
            return _chatRepository.save(chat);
        }else {
            throw new Exception("ERROR : TOKEN is INCORRECT");
        }

    }
}
