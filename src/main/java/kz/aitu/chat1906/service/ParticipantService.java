package kz.aitu.chat1906.service;

import kz.aitu.chat1906.model.Participant;
import kz.aitu.chat1906.model.User;
import kz.aitu.chat1906.repository.IAuthRepository;
import kz.aitu.chat1906.repository.IParticipantRepository;
import kz.aitu.chat1906.repository.IUserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ParticipantService {
    private IParticipantRepository _participantRepository;
    private IUserRepository _userRepository;
    private IAuthRepository _authRepository;


    public List<Participant> getAll(String token) throws Exception {
        if (_authRepository.existsByToken(token)) {
            return _participantRepository.findAll();
        }else {
            throw new Exception("ERROR : TOKEN is INCORRECT");
        }
    }

    public List<User> getUsersByChatId(Long id, String token) throws Exception {

        if (_authRepository.existsByToken(token)) {
            List<Participant> participantList = _participantRepository.findParticipantsByChatId(id);
            List<User> userList = new LinkedList<User>();
            for (Participant participant : participantList){
                Optional<User> optionalUser =  _userRepository.findById(participant.getUserId());
                optionalUser.ifPresent(userList::add);
            }
            return userList;
        }else {
            throw new Exception("ERROR : TOKEN is INCORRECT");
        }
    }

    public Participant addParticipant( Participant participant, String token) throws Exception {
        if (_authRepository.existsByToken(token)) {
            return _participantRepository.save(participant);

        }else {
            throw new Exception("ERROR : TOKEN is INCORRECT");
        }
    }

    public Participant updateParticipantWithId( Participant participant, String token) throws Exception {

        if (_authRepository.existsByToken(token)) {
            return  _participantRepository.save(participant);
        }else {
            throw new Exception("ERROR : TOKEN is INCORRECT");
        }
    }

    public String deleteById( Long id, String token) throws Exception {

        if (_authRepository.existsByToken(token)) {
            _participantRepository.deleteById(id);
            return "Success deleted";        }
        else {
            throw new Exception("ERROR : TOKEN is INCORRECT");
        }
    }

    public String deleteAll(String token) throws Exception {
        if (_authRepository.existsByToken(token)) {
            _participantRepository.deleteAll();
            return "All droped";       }
        else {
            throw new Exception("ERROR : TOKEN is INCORRECT");
        }

    }
}



