package kz.aitu.chat1906.service;


import kz.aitu.chat1906.model.Auth;
import kz.aitu.chat1906.model.modelDTO.AuthDTO;
import kz.aitu.chat1906.repository.IAuthRepository;
import kz.aitu.chat1906.repository.IUserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

@Service
@AllArgsConstructor
public class AuthService {

    private IAuthRepository _authRepository;
    private IUserRepository _userRepository;

    public Auth auth(AuthDTO user) throws Exception {

        Auth userFromDB = _authRepository.getByLogin(user.getLogin());

        if (userFromDB != null){
            if(userFromDB.getPassword().equals(user.getPassword())){
                String token = getToken(user);
                userFromDB.setToken(token);
                userFromDB.setLastLoginTimestamp(getCurrentDate());
            }else {
                throw new Exception("ERROR : INCORRECT PASSWORD");
            }
        }else {
            throw new Exception("ERROR : SUCH USER DOESNT EXIST");
        }

        _authRepository.save(userFromDB);
        return userFromDB;
    }

    public String register(AuthDTO user) throws Exception {

        if (user.getPassword() != null && user.getLogin() != null ){
            Auth newUser = new Auth();
            newUser.setLogin(user.getLogin());
            newUser.setPassword(user.getPassword());
            newUser.setLastLoginTimestamp(getCurrentDate());
            newUser.setUserId(_userRepository.getByName(user.getLogin()));
            return "Succesfully created";
        }else {
            throw  new Exception("Your password or login null");
        }
    }

    private String getToken(AuthDTO user){
        Calendar cal = Calendar.getInstance();
        String token = UUID.randomUUID().toString().toUpperCase()
                +"-" + user.getPassword().getBytes()+"-"
                +cal.getTimeInMillis();
        return token;
    }

    private Long getCurrentDate() {
        Date date = new Date();
        return date.getTime();
    }

}
