package kz.aitu.chat1906.service;

import kz.aitu.chat1906.model.User;
import kz.aitu.chat1906.repository.IAuthRepository;
import kz.aitu.chat1906.repository.IUserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
@AllArgsConstructor
public class UserService {
    private IUserRepository _userRepository;
    private IAuthRepository _authRepository;


    public List<User> getAll(String token) throws Exception {
        if (_authRepository.existsByToken(token)) {
            return _userRepository.findAll();
        } else {
            throw new Exception("ERROR : TOKEN is INCORRECT");
        }
    }



    public User newUser(User user,String token) throws Exception {
        if (_authRepository.existsByToken(token)) {
            _userRepository.save(user);
            return user;
        } else {
            throw new Exception("ERROR : TOKEN is INCORRECT");
        }

    }

    public String updateUser(User user,String token) throws Exception {
        if (_authRepository.existsByToken(token)) {
            _userRepository.save(user);
            return "Updated" + user;
        } else {
            throw new Exception("ERROR : TOKEN is INCORRECT");
        }
    }

    public String deleteUserById(Long id,String token) throws Exception {
        if (_authRepository.existsByToken(token)) {
            _userRepository.deleteById(id);
            return "Succesfull deleted";
        } else {
            throw new Exception("ERROR : TOKEN is INCORRECT");
        }
    }
}
