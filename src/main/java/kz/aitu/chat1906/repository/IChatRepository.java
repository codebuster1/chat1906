package kz.aitu.chat1906.repository;

import kz.aitu.chat1906.model.Chat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface IChatRepository extends JpaRepository<Chat , Long> {
}
