package kz.aitu.chat1906.repository;

import kz.aitu.chat1906.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IUserRepository extends JpaRepository<User,Long> {
    Long getByName(String name);
}
