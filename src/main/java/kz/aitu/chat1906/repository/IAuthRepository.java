package kz.aitu.chat1906.repository;

import kz.aitu.chat1906.model.Auth;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IAuthRepository extends JpaRepository<Auth,Long> {


    Auth getByLogin(String login);
    Boolean existsByToken(String token);
}
