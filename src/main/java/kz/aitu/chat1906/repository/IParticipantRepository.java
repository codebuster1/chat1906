package kz.aitu.chat1906.repository;

import kz.aitu.chat1906.model.Participant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import java.util.List;

@Repository
public interface IParticipantRepository extends JpaRepository<Participant , Long> {

    List<Participant> findParticipantsByChatId(Long id);
    List<Participant> findParticipantsByUserId(Long id);
    Boolean existsByUserIdAndChatId(Long userId,Long chatId);
}
