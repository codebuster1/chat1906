package kz.aitu.chat1906.repository;

import kz.aitu.chat1906.model.Message;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


import java.util.List;

@Repository
public interface IMessageRepository extends JpaRepository<Message,Long> {

    List<Message> findMessagesByChatId(Long chatId);

    @Query(value = "SELECT * FROM message m " +
            "WHERE m.chat_id = ?1 " +
            "ORDER BY m.id DESC LIMIT 10", nativeQuery=true)
    List<Message> getLastTenMessagesByChatId(Long chatId);

    @Query(value = "SELECT * FROM message m" +
            " WHERE m.user_id = ?1 " +
            "ORDER BY m.id DESC LIMIT 10", nativeQuery=true)
    List<Message> getLastTenMessageByUserId(Long userId);

    List<Message> findByChatId(Long chatId , Pageable pageable);

    Message findMessageByUserIdAndId(Long userId , Long messageId);

    List<Message> getByDeliveredIsFalse();
}
