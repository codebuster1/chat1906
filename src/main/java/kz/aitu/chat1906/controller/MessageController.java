package kz.aitu.chat1906.controller;


import kz.aitu.chat1906.model.Message;
import kz.aitu.chat1906.service.MessageService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/messages")
public class MessageController {

    private MessageService _messageService;

    @GetMapping("/getAll/userId/{userId}")
    public ResponseEntity<?> getAll(@PathVariable Long userId, @RequestHeader String token) throws Exception {
        return ResponseEntity.ok(_messageService.getAll(userId , token));
    }

    @GetMapping("/getTenMessageByChatId/{id}")
    public ResponseEntity<?> getTenMessageByChatId(@PathVariable Long id, @RequestHeader String token) throws Exception {
        return ResponseEntity.ok(_messageService.getLastTenMessageByChatId(id, token));
    }

    @GetMapping("/getAllNotDelivered")
    public ResponseEntity<?> getNotDelivered(@RequestHeader String token) throws Exception {
        return  ResponseEntity.ok(_messageService.getNotDelivered(token));
    }

    @GetMapping("/getTenMessageByUserId/{id}")
    public ResponseEntity<?> getTenMessageByUserId(@PathVariable Long id,@RequestHeader String token) throws Exception {
        return ResponseEntity.ok(_messageService.getLastTenMessageByUserId(id, token));
    }

    @GetMapping("/chat/{id}")
    public ResponseEntity<?> getMessagesByChatId(@PathVariable Long id , @RequestParam(value = "size",required = false) Integer size, @RequestHeader String token) throws Exception {
        return ResponseEntity.ok(_messageService.getMessagesByChatId(id,size,token));
    }

    @PostMapping("")
    public ResponseEntity<?> addMessage(@RequestBody Message message,@RequestHeader String token) throws Exception {
        return ResponseEntity.status(HttpStatus.CREATED).body(_messageService.addMessage(message,token));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteMessageById(@PathVariable Long id, @RequestHeader String token) throws Exception {

        return ResponseEntity.ok(_messageService.deleteMessageById(id,token));
    }

    @PutMapping("/isRead")
    public ResponseEntity<?> changeIsReadStatus(@RequestBody  Message message ,@RequestHeader String token) throws Exception {
        return  ResponseEntity.ok(_messageService.changeIsReadStatus(message.getUserId(),message.getId(),message.getRead(),token));
    }

    @PutMapping("")
    public ResponseEntity<?> updateMessageById(@RequestBody Message message, @RequestHeader String token) throws Exception {
        return ResponseEntity.ok(_messageService.updateMessageById(message, token));
    }

}
