package kz.aitu.chat1906.controller;

import kz.aitu.chat1906.model.Chat;
import kz.aitu.chat1906.model.modelDTO.ReturnAuthDTO;
import kz.aitu.chat1906.service.ChatService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/chats")
public class ChatController {

    private ChatService _chatService;

    @GetMapping("")
    public ResponseEntity<?> getChats(@RequestHeader String token ) throws Exception {
        return  ResponseEntity.ok(_chatService.getChats(token));
    }

    @GetMapping("/getChats/user/{id}")
    public ResponseEntity<?> getChatsByUserId(@PathVariable Long id , @RequestHeader String token) throws Exception {
        return  ResponseEntity.ok(_chatService.getChatsByUserId(id, token));
    }


    @PostMapping("")
    public ResponseEntity<?> addChat(@RequestBody Chat chat, @RequestHeader String token) throws Exception {
        return  ResponseEntity.ok(_chatService.addChat(chat, token));
    }

    @DeleteMapping("/{id}")
    public  ResponseEntity<?> deleteChatById(@PathVariable Long id , @RequestHeader String token) throws Exception {
        return ResponseEntity.ok(_chatService.deleteChatById(id, token));
    }

    @DeleteMapping("/deleteAll")
    public ResponseEntity<?> deleteAll(@RequestHeader String token) throws Exception {
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(_chatService.deleteAll(token));
    }

    @PutMapping("")
    public  ResponseEntity<?> updateById(@RequestBody Chat chat, @RequestHeader String token) throws Exception {
        return ResponseEntity.ok(_chatService.updateById(chat, token));
    }
}
