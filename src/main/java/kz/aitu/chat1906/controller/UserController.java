package kz.aitu.chat1906.controller;

import kz.aitu.chat1906.model.User;
import kz.aitu.chat1906.repository.IUserRepository;

import kz.aitu.chat1906.service.UserService;
import lombok.AllArgsConstructor;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/users")
public class UserController {

    private UserService _userService;

    @GetMapping("")
    public ResponseEntity<?> getAll(@RequestHeader String token) throws Exception {
        return ResponseEntity.ok(_userService.getAll(token));
    }

    @PostMapping("")
    public ResponseEntity<?> newUser(@RequestBody User user, @RequestHeader String token) throws Exception {
        return ResponseEntity.ok(_userService.newUser(user,token));
    }


    @PutMapping("")
    public ResponseEntity<?> updateUser(@RequestBody User user, @RequestHeader String token) throws Exception {

        return ResponseEntity.ok(_userService.updateUser(user,token));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteUserById(@PathVariable Long id, @RequestHeader String token) throws Exception {

        return ResponseEntity.ok(_userService.deleteUserById(id,token));
    }
}