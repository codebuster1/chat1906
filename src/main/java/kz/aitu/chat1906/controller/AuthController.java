package kz.aitu.chat1906.controller;


import kz.aitu.chat1906.model.modelDTO.AuthDTO;
import kz.aitu.chat1906.service.AuthService;
import kz.aitu.chat1906.service.UserService;
import lombok.AllArgsConstructor;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/auth")

public class AuthController {

    private AuthService _authService;

    @PostMapping("/auth")
    public ResponseEntity<?> authUser(@RequestBody AuthDTO user) throws Exception {
        return ResponseEntity.ok(_authService.auth(user));
    }

    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody AuthDTO user )throws  Exception{
        return ResponseEntity.ok(_authService.register(user));
    }

}
