package kz.aitu.chat1906.controller;

import kz.aitu.chat1906.model.Participant;
import kz.aitu.chat1906.service.ParticipantService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/participants")
public class ParticipantController {

    private ParticipantService _participantService;

    @GetMapping("/getAll")
    public ResponseEntity<?> getAll(@RequestHeader String token) throws Exception {

        return ResponseEntity.ok(_participantService.getAll(token));
    }

    @GetMapping("/getUsers/chat/{id}")
    public ResponseEntity<?> getUserByChatId(@PathVariable Long id,@RequestHeader String token) throws Exception {
        return ResponseEntity.ok(_participantService.getUsersByChatId(id, token));
    }
    @PostMapping("")
    public ResponseEntity<?> addParticipant(@RequestBody Participant participant, @RequestHeader String token) throws Exception {
        return ResponseEntity.ok(_participantService.addParticipant(participant,token));
    }

    @PutMapping("")
    public  ResponseEntity<?> updateParticipantWithId(@RequestBody Participant participant, @RequestHeader String token) throws Exception {
        return ResponseEntity.ok(_participantService.updateParticipantWithId(participant, token));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteById(@PathVariable Long id, @RequestHeader String token) throws Exception {

        return ResponseEntity.ok(_participantService.deleteById(id, token));
    }

    @DeleteMapping("")
    public ResponseEntity<?> deleteAll(@RequestHeader String token) throws Exception {
        _participantService.deleteAll(token);
        return ResponseEntity.ok("All droped");
    }

}
