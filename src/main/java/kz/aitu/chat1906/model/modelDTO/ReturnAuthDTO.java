package kz.aitu.chat1906.model.modelDTO;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReturnAuthDTO {
    private String login;
    private Long userId;
    private String token;
}
